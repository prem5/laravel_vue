
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import Datepicker from 'vuejs-datepicker';
window.Vue.use(VueRouter);
window.Vue.use(VeeValidate);
window.Vue.component('my-date',Datepicker);


import App from './App.vue';
import MemberIndex from './components/members/MembersIndex.vue';
import MemberCreate from './components/members/MembersCreate.vue';
import MemberEdit from './components/members/MembersEdit.vue';
import MemberShow from './components/members/MembersShow';

const routes = [
    {path: '/',component:MemberIndex,name:'listMembers'},
    {path: '/member/create',component:MemberCreate,name:'createMember'},
    {path: '/member/edit/:id',component:MemberEdit, name:'editMember'},
    {path: '/member/show/:id',component:MemberShow, name:'showMember'}

]

const router = new VueRouter({routes})
const app = new Vue({
    el:'#members',
    router,
    components:{
        MemberIndex,
        MemberCreate,
        MemberEdit,
        Datepicker,
        MemberShow
    },
    render: h => h(App),
})
