## Laravel 5.6 + Vue.js - Simple form implementation.

Simple project showing how we can store data into  database and show/edit/delete that data.



---

### How to use

- Clone the repository with git clone https://bitbucket.org/prem5/laravel_vue
- move inside the laravel_vue folder.
- Copy __.env.example__ file to __.env__ and edit database credentials there and create database in your mysql.
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- Run __php artisan serve__
- That's it - load the homepage, then click on Register and fill the form and get registerd.
- Login with user emain and password you have given in registration page.
- After login you can do crud of Member.[I have putted all suggested  form fields in member creation form]

