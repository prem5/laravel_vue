<?php

namespace App\Http\Controllers\Api\V1;

use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::all();
        return response()->json(['data'=>$members],200);
    }

    public function show($id)
    {
        $member = Member::findOrFail($id);
        return response()->json(['data'=>$member],200);
    }

    public function update(Request $request, $id)
    {
        \request()->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required|unique:members,email,'.$id,
            'gender'=>'required',
            'dob'=>'required',
        ],[
            'dob.required'=>' Date of birth is required'
        ]);
        $member  = Member::findOrFail($id);
        $member->name = \request('name');
        $member->phone = \request('phone');
        $member->email = \request('email');
        $member->gender = \request('gender');
        $member->dob = date('Y-m-d',strtotime(substr(\request('dob'),0,15)));
        $member->biography = \request('biography');
        if(\request()->hasFile('picture')){
            $file_name = time().rand(00000,99999).'.'.\request()->file('picture')->getClientOriginalExtension();
            \request()->file('picture')->move('uploads',$file_name);
            $member->picture = 'uploads/'.$file_name;
        }
        $member->save();
        return response()->json(['data'=>$member],200);
    }

    public function store(Request $request)
    {
        \request()->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required|unique:members,email',
            'gender'=>'required',
            'dob'=>'required',
        ],[
            'dob.required'=>' Date of birth is required'
        ]);
        $member  = new Member;
        $member->name = \request('name');
        $member->phone = \request('phone');
        $member->email = \request('email');
        $member->gender = \request('gender');
        $member->dob = date('Y-m-d',strtotime(substr(\request('dob'),0,15)));
        $member->biography = \request('biography');
        if(\request()->hasFile('picture')){
            $file_name = time().rand(00000,99999).'.'.\request()->file('picture')->getClientOriginalExtension();
            \request()->file('picture')->move('uploads',$file_name);
            $member->picture = 'uploads/'.$file_name;
        }
        $member->save();
        return response()->json(['data'=>$member],201);
    }

    public function destroy($id)
    {
        $member = Member::findOrFail($id);
        if(file_exists($member->picture) && is_file($member->picture)){
            unlink($member->picture);
        }
        $member->delete();
        return response()->json(['data'=>$id],201);
    }
}
